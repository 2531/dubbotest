const nzd = require('node-zookeeper-dubbo');

var express = require('express');
var router = express.Router();

const userDto = {
    $class: 'com.cifaz.test.dubbo.api.UserDto',
    $: {
        userName: "nihaoaaaa",
        passwd: 'test'
    }
};

const opt = {
    application: {name: 'fxxk'},
    register: '127.0.0.1:2181',
    dubboVer: '2.6.0',
    root: 'dubbo',
    dependencies: {
        testService: {
            interface: 'com.cifaz.test.dubbo.api.ITestService',
            // version: 'LATEST',
            timeout: 6000,
            // group: 'dubbo',
            methodSignature: {
                hi: (id) => [{'$class': 'java.lang.Long', '$': id}]
                ,getUserDto: (userDto) => [userDto]
                // findByName : (name) => (java) => [ java.String(name) ],
            }
        }
    }
}

opt.java = require('js-to-java');
const Dubbo = new nzd(opt);

/* GET home page. */
router.get('/', function (req, res, next) {

    Dubbo.testService.hi(1).then(function (data) {
        console.log(data);
        console.log(data.userName);
    })

    console.log("参数:" +  JSON.stringify(userDto));
    Dubbo.testService.getUserDto(userDto).then(function (data) {
        console.log(">>>>>>>" + data.userName);
    })

    res.render('index', {title: 'Express'});

});

module.exports = router;
